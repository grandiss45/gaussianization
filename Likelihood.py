__author__ = 'sebastiangrandis'

import numpy as np
import scipy.linalg as la


def BoxCox(x, params):
    a, lam = params
    if (x+a).any < 0:
        raise ValueError('invalid offset a')
    if lam == 0:
        return np.log(x+a)
    else:
        return 1./lam*(x+a)**lam - 1


def diff_BoxCox(x, params):
    a, lam = params
    if (x+a).any < 0:
        raise ValueError('invalid offset a')
    return (x+a)**(lam-1)


def BoxCox_ndim(sample, params):
    ndim, N_sample = np.shape(sample)
    params = params.reshape(ndim, -1)

    y = np.empty((ndim, N_sample))

    for i in xrange(ndim):
        y[i] = BoxCox(sample[i], params[i])

    return y


def invBoxCox(x, params):
    a, lam = params
    if lam == 0:
        return np.exp(x) - a
    else:
        return (lam*(x+1))**(1./lam) - a


def invBoxCox_ndim(sample, params):
    ndim, N_sample = np.shape(sample)
    params = params.reshape(ndim, -1)

    y = np.empty((ndim, N_sample))

    for i in xrange(ndim):
        y[i] = invBoxCox(sample[i], params[i])

    return y


def Arsinh(x, params):
    a, t = params
    if t > 0:
        return 1./t*np.sinh(t*(x-a))
    elif t == 0:
        return x-a
    else:
        return 1./t*np.arcsinh(t*(x-a))


def invArsinh(x, params):
    a, t = params
    if t > 0:
        return 1./t*np.arcsinh(t*x)+a
    elif t == 0:
        return x+a
    else:
        return 1./t*np.sinh(t*x)+a


def invArsinh_ndim(sample, params):
    ndim, N_sample = np.shape(sample)
    params = params.reshape(ndim, -1)

    y = np.empty((ndim, N_sample))

    for i in xrange(ndim):
        y[i] = invArsinh(sample[i], params[i])

    return y


def diff_Arsinh(x, params):
    a, t = params
    if t < 0:
        return 1./np.sqrt(1.+(t*(x-a))**2)
    else:
        return np.cosh(t*(x-a))


def Arsinh_ndim(sample, params):

    ndim, N_sample = np.shape(sample)
    params = params.reshape(ndim, -1)

    y = np.empty((ndim, N_sample))

    for i in xrange(ndim):
        y[i] = Arsinh(sample[i], params[i])

    return y


class Likelihood_after_Gaussianization(object):
    """
    This class wraps a likelihood estimator for a sample which has been Gaussianized as described in Grandis et al 2016
    """
    def __init__(self, path):
        """
        :param path: path in which the Gaussianization parameters and the weights of the chain are store
        :return: initializes the an instance of the Likelihood estimator
        """

        self.path = path

        self.trans_params_1 = np.genfromtxt(self.path+'step_1.txt')
        self.trans_params_2 = np.genfromtxt(self.path+'step_2.txt')
        self.trans_params_Arsinh = np.genfromtxt(self.path+'step_Arsinh.txt')

        self.mu_PCA = np.genfromtxt(self.path+'mu_PCA.txt')
        cov_PCA = np.genfromtxt(self.path+'cov_PCA.txt')
        self.half_cov_PCA = la.cholesky(cov_PCA)

        self.mu_final = np.genfromtxt(self.path+'mu_final_1.txt')
        self.cov_final = np.genfromtxt(self.path+'cov_final_1.txt')

        self.linear_map = np.genfromtxt(self.path+'linear_map.txt')

        weights = np.genfromtxt(self.path+'weights_1.txt')
        self.W1 = np.sum(weights)
        self.W2 = np.sum(weights**2)
        return

    def setup(self):
        """
        :return: This section is needed if you want to add an instance of this class to a CosmoHammer chain
        """
        return

    def computeLikelihood(self, ctx):
        """
        :param ctx: CosmoHammer Chain Context
        :return: This section is needed if you want to add an instance of this class to a CosmoHammer chain
        """
        params = ctx.getParams()

        try:
            return self.Likelihood(params)
        except ValueError:
            return -np.inf

    def create_sample(self, num=5000):
        """
        :param num: integer
        :return: creates a sample of length num from the likelihood. Attention: for some points outside the approx 8
        sigma region of the underlying sample the BoxCox Transformations are not defined.
        """

        X = np.random.multivariate_normal(self.mu_final, self.cov_final, num).T

        step2 = invBoxCox_ndim(X, self.trans_params_2)
        step_Arsinh = invArsinh_ndim(step2, self.trans_params_Arsinh)
        invPCA = np.dot(self.half_cov_PCA.T, step_Arsinh)+self.mu_PCA[:, None]
        step1 = invBoxCox_ndim(invPCA, self.trans_params_1)

        #inds = np.where(np.isfinite(step1))[1]

        return step1#[:, inds]

    def Likelihood(self, params):
        """
        :param params: parameters at which you want to evaluate the likelihood function
        :return: value of the likelihood. Attention: for some points outside the approx 8 sigma region of the underlying
        sample the BoxCox Transformations are not defined. In those cases, the likelihood returns -inf.
        """

        ndim = len(params)
        params_reshape = params.reshape(ndim, 1)
        params_linear_map = np.dot(self.linear_map, params_reshape)
        params_after_step_1 = BoxCox_ndim(params_linear_map, self.trans_params_1)
        if np.isnan(params_after_step_1).any():
            result = -np.inf
        else:
            residual_PCA = params_after_step_1-self.mu_PCA[:, None]
            params_after_PCA = la.solve(self.half_cov_PCA.T, residual_PCA)
            params_after_Arsinh_step = Arsinh_ndim(params_after_PCA, self.trans_params_Arsinh)
            params_after_step_2 = BoxCox_ndim(params_after_Arsinh_step, self.trans_params_2)

            if np.isnan(params_after_step_2).any():
                result = -np.inf
            else:

                residual_final = params_after_step_2 - self.mu_final[:, None]
                T_squared = np.dot(residual_final.T, la.solve(self.cov_final, residual_final))
                result = -0.5*self.W1*np.log(1+T_squared*self.W1/(self.W1**2-self.W2))
                result = result[0, 0]

        return result


class Gaussian_Likelihood(object):
    """
    This class wraps a likelihood estimator for a sample which is already Gaussian
    """

    def __init__(self, path):
        """
        :param path: path to the repository where the mean, covariance and weights of the sample are stored
        :return: instance of Gaussian Likelihood
        """

        self.path = path

        self.mu_final = np.genfromtxt(self.path+'mu_final_1.txt')
        self.cov_final = np.genfromtxt(self.path+'cov_final_1.txt')

        weights = np.genfromtxt(self.path+'weights_1.txt')
        self.W1 = np.sum(weights)
        self.W2 = np.sum(weights**2)
        return

    def setup(self):
        """
        :return: This section is needed if you want to add an instance of this class to a CosmoHammer chain
        """
        return

    def computeLikelihood(self, ctx):
        """
        :param ctx: CosmoHammer Chain Context
        :return: This section is needed if you want to add an instance of this class to a CosmoHammer chain
        """
        params = ctx.getParams()

        try:
            return self.Likelihood(params)
        except ValueError:
            return -np.inf

    def Likelihood(self, params):
        """
        :param params: parameters at which you want to evaluate the likelihood function
        :return: value of the likelihood.
        """

        residual_final = params - self.mu_final
        T_squared = np.dot(residual_final, la.solve(self.cov_final, residual_final))
        result = -0.5*self.W1*np.log(1+T_squared*self.W1/(self.W1**2-self.W2))

        return result

    def create_sample(self, num=5000):
        """
        :param num: integer
        :return: creates a sample of length num from the likelihood
        """

        X = np.random.multivariate_normal(self.mu_final, self.cov_final, num).T

        return X

