__author__ = 'sebastiangrandis'

########################################################################################################################
# Short Demo
########################################################################################################################

import numpy as np
import matplotlib.pyplot as plt
# contour plotting routines used by many cosmologists:
# http://getdist.readthedocs.org/en/latest/
import getdist as gd
from getdist import plots, MCSamples

# the two likelihood modules we present
from Likelihood import Likelihood_after_Gaussianization, Gaussian_Likelihood

########################################################################################################################

# This script provides a short demo of the Likelihood modules we present in \textt{Likelihood.py}. We provide an
# analytical approximation to the Planck 15 fiducial CMB constraints on the cosmological parameters in the models:
# flat LCDM, curved LCDM, and flat +AL LCDM. For further details, see Grandis et al. 2016.

########################################################################################################################
### flat LCDM

fl_ldcm_path = 'TT_lowTEB/base/'
fl_ldcm_like = Gaussian_Likelihood(fl_ldcm_path)

# evaluate likelihood in point of parameter space
theta = np.array([68, 0.02, 0.11, 0.96, 3.1])
print 'flat LCDM, likelihood of points', theta, 'is', fl_ldcm_like.Likelihood(theta)

# creating a sample
fl_ldcm_sample = fl_ldcm_like.create_sample(num=10000)

names = ['H_0', '\omega_b', '\omega_{cdm}', 'n_S', '\ln(10^{10}\,A_S)']
labels = names
analytical = MCSamples(samples=fl_ldcm_sample.T,
                       names=names, labels=labels)
original = MCSamples(samples=np.genfromtxt(fl_ldcm_path+'chain_1.txt'),
                     weights=np.genfromtxt(fl_ldcm_path+'weights_1.txt'),
                       names=names, labels=labels)

g = plots.getSubplotPlotter()
g.triangle_plot([analytical, original], filled=True,
                legend_labels=['analytical', 'original'])
plt.savefig('flat_LCDM_compare.pdf')

########################################################################################################################
### curved LCDM

curv_ldcm_path = 'TT_lowTEB/omegak/'
curv_ldcm_like = Likelihood_after_Gaussianization(curv_ldcm_path)

# evaluate likelihood in point of parameter space
theta = np.array([5.45e+01, 2.235e-02, 1.21e-01, 9.67e-01, 3.03, -3.79e-02])
# do not move too far from the degenerancy, otherwise the result will be -inf
print 'curved LCDM, likelihood of points', theta, 'is', curv_ldcm_like.Likelihood(theta)

# creating a sample
curv_ldcm_sample = curv_ldcm_like.create_sample(num=10000)

names = ['H_0', '\omega_b', '\omega_{cdm}', 'n_S', '\ln(10^{10}\,A_S)', '\Omega_K']
labels = names
analytical = MCSamples(samples=curv_ldcm_sample.T,
                       names=names, labels=labels)
original = MCSamples(samples=np.genfromtxt(curv_ldcm_path+'chain_1.txt'),
                     weights=np.genfromtxt(curv_ldcm_path+'weights_1.txt'),
                       names=names, labels=labels)

g = plots.getSubplotPlotter()
g.triangle_plot([analytical, original], filled=True,
                legend_labels=['analytical', 'original'])
plt.savefig('curved_LCDM_compare.pdf')
# if this plot does not work, please try to draw the sample again: you might
# have picked a point outside the support of my approximation.

########################################################################################################################
### flat $+A_L$ $\Lambda$CDM

al_ldcm_path = 'TT_lowTEB/Alens/'
al_ldcm_like = Likelihood_after_Gaussianization(al_ldcm_path)

# evaluate likelihood in point of parameter space
theta = np.array([6.94e+01, 2.28e-02, 1.15e-01, 9.79e-01, 3.08, 1.23])
print 'flat +AL LCDM, likelihood of points', theta, 'is', curv_ldcm_like.Likelihood(theta)

# creating a sample
al_ldcm_sample = al_ldcm_like.create_sample(num=10000)

names = ['H_0', '\omega_b', '\omega_{cdm}', 'n_S', '\ln(10^{10}\,A_S)', 'A_L']
labels = names
analytical = MCSamples(samples=al_ldcm_sample.T,
                       names=names, labels=labels)
original = MCSamples(samples=np.genfromtxt(al_ldcm_path+'chain_1.txt'),
                     weights=np.genfromtxt(al_ldcm_path+'weights_1.txt'),
                       names=names, labels=labels)

g = plots.getSubplotPlotter()
g.triangle_plot([analytical, original], filled=True,
                legend_labels=['analytical', 'original'])
plt.savefig('flat+al_LCDM_compare.pdf')
# if this plot does not work, please try to draw the sample again: you might
# have picked a point outside the support of my approximation.

plt.show()